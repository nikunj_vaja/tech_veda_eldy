#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int fd, i;
	unsigned char *devbuf, *temp;
	char str[512];
	fd = open("/dev/vDev-1", O_RDWR);

	devbuf = (char *)mmap((void *)0, 512, PROT_READ | PROT_WRITE,
				MAP_SHARED, fd, 0);
	if (devbuf == NULL) {
		perror("Maping Failed");
		exit(1);
	}
	printf("[%d] device buf mapped @ %p\n",getpid(),devbuf);
	strncpy(str,devbuf, 512);
	printf("[%d] Data@devbuf %s\n",getpid(),str);
	getchar();

	i = munmap(devbuf, 512);
	if (i != 0)
		printf(" failed to unmap\n");
	return 0;
}
