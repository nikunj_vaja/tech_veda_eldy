#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

int fd;
char my_buf[512];

/* signal handler that is called when sigio is got */
void handler(int sig)
{
	printf("Got SIGIO\n");
	printf("Attempting to read from the device\n");
	read( fd, my_buf, 64 );
	printf("Read [%s]\n", my_buf);
}

int main()
{
	int i, oflags;
	char cmd[250];
	
	bzero(my_buf, 512);

	/* open the device for read/write/lseek */
	printf("[%d] - Accessing vDev-1\n", getpid() );
	fd = open( "/dev/vDev-1", O_RDWR );
	if( fd < 0 ) {
		printf("Device could not be opened\n");
		return 1;
	}
	printf("Device opened with ID [%d]\n", fd);

	fcntl(fd, F_SETOWN, getpid()); /* set this process as owner */
	oflags = fcntl(fd, F_GETFL); /* get file flags now */
	fcntl(fd, F_SETFL, oflags|FASYNC); /* enable async notice */		
	signal(SIGIO, handler); /* install signal handler */
	
	printf("Enter a shell command\n");
	while(1) {
		bzero(cmd, 250);
		printf("\nEnter Command: ");
		fgets(cmd, 250, stdin );
		if(!strcmp("exit", cmd) ) break;
		system( cmd );
	}
	close(fd);
	return 0;
}


